import http from 'http';

export default interface IRequest extends http.IncomingMessage {
    params: any;
}