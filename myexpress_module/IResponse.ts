import http from 'http';

export default interface IResponse extends http.ServerResponse {
    json(object: Object): any;
    send(content: string): any;
    error(content: string): any;
    user(id: number, limit?: number, status?: string)
}