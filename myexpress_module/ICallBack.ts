import {IncomingMessage, ServerResponse} from "http";
import IResponse from "./IResponse";

export default interface ICallBack{
    (
        req?: IncomingMessage,
        res?: IResponse,
        next?: ICallBack,
        err?: NodeJS.ErrnoException,
        html?: ServerResponse
    ) : void

}
