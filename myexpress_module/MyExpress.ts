import http from 'http';
import IRoutes from "./IRoutes";
import {existsSync, readFileSync} from "fs";
import IResponse from "./IResponse";
import ICallBack from "./ICallBack";
import path from 'path';
import IRequest from "./IRequest";

class MyExpress{

    [x: string]: any;
    private routes: IRoutes[] = [];
    private server: http.Server;


    constructor(){

        for (const method of ['GET', 'POST', 'PUT', 'DELETE', 'ALL']){
            this[method.toLowerCase()] = (path: string, cb: ICallBack) => {
                const path_without_params = path.split('?')
                const path_array = path_without_params[0].match(/((\/)(:?\w+(\(.+\))?))/g);
                let myFuturRegex = '';
                path_array.forEach(elem => {
                    if (!elem.match(/:/g)){
                        myFuturRegex += elem;
                    } else {
                        let maVarName = elem.replace(':','');
                        maVarName = maVarName.replace('/','');
                        myFuturRegex += `:?/?(?<${maVarName}>.+)`;
                    }
                });
                const myRegex = new RegExp(myFuturRegex, 'g');
                this.routes.push({method: method, pathRegexp: myRegex, callback: cb})
            }
        }



        this.server = http.createServer( (req: http.IncomingMessage, res: http.ServerResponse) => {
            let request: IRequest = this.handleRequest(req);
            let response: IResponse = this.handleResponse(res);

            let route = this.routes.find( (route) => {
                return  (route.method === req.method || route.method === 'ALL') && route.pathRegexp.test(req.url);
            });

            if(route){
                console.log(this.routes);
                console.log(req.url.match(route.pathRegexp).groups);
                request.params = {
                    ...req.url.match(route.pathRegexp).groups
                }
                route.callback(request, response);
            } else {
                res.writeHead(404, {'Content-Type': 'text/html'});
                res.write('no implemented');
                res.end();
            }
        })
    }



    listen(port: number, callback: ICallBack){
        this.server.listen(port, callback);
    }

    render(file: string, values: any, cb: Function){
        const path_file = path.join(process.cwd(), 'static', `${file}.html`);

        if (!existsSync(path_file)){
            cb(new Error('marche pas'), null)
        } else {
            const content = readFileSync(path_file, 'utf-8');


            cb(null, this.contentProcess(content, values))
        }

    }

    contentProcess(content: string, values: any){
        const regex = /{{ ?(\w+)(( ?[|] ?)((\w+)(\:(\d+))?))? ?}}/gi;
        let contentProcessed = content.replace(
            regex,
            (item: string, ...args: any[]): string => {
                const [key, , pipe, , transform, , limit] = args;

                const v = values[key];

                if(!v) return 'undefined';

                if (pipe && transform){
                    const func = this[transform];
                    if (transform === 'fixed') return func(v, limit);
                    return func ? func(v) : v;
                } else {
                    return v;
                }
            }
        );
        return contentProcessed;
    }


    upper(content: string): string{
        return content.toUpperCase();
    }

    lower(content: string): string{
        return content.toLowerCase();
    }

    fixed(content: string, limit: number){
        const tmp = parseFloat(content);
        return tmp.toFixed(limit);
    }

    handleRequest(req: http.IncomingMessage){
        let request = req as IRequest;
        return request;
    }

    handleResponse(res: http.ServerResponse){
        let response = res as IResponse;
        response.json = (item: any) => {
            response.setHeader('Content-Type', 'application/json');
            response.write(JSON.stringify(item));
            response.end();
        };
        response.send = (content: string) => {
            response.setHeader('Content-Type', 'text/html')
            response.write(content);
            response.end();
        };
        response.error = (content: string) => {
            response.setHeader('Content-Type', 'text/html')
            response.write(content);
            response.end();
        }
        return response
    }

}

export default function express(){
    return new MyExpress();
}