import ICallBack from "./ICallBack";

export default interface IRoute  {
    method: string,
    pathRegexp: RegExp,
    callback: ICallBack,

}