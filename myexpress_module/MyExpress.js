"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var http_1 = __importDefault(require("http"));
var fs_1 = require("fs");
var path_1 = __importDefault(require("path"));
var MyExpress = /** @class */ (function () {
    function MyExpress() {
        var _this = this;
        this.routes = [];
        var _loop_1 = function (method) {
            this_1[method.toLowerCase()] = function (path, cb) {
                var path_without_params = path.split('?');
                var path_array = path_without_params[0].match(/((\/)(:?\w+(\(.+\))?))/g);
                var myFuturRegex = '';
                path_array.forEach(function (elem) {
                    if (!elem.match(/:/g)) {
                        myFuturRegex += elem;
                    }
                    else {
                        var maVarName = elem.replace(':', '');
                        maVarName = maVarName.replace('/', '');
                        myFuturRegex += ":?/?(?<" + maVarName + ">.+)";
                    }
                });
                var myRegex = new RegExp(myFuturRegex, 'g');
                _this.routes.push({ method: method, pathRegexp: myRegex, callback: cb });
            };
        };
        var this_1 = this;
        for (var _i = 0, _a = ['GET', 'POST', 'PUT', 'DELETE', 'ALL']; _i < _a.length; _i++) {
            var method = _a[_i];
            _loop_1(method);
        }
        this.server = http_1["default"].createServer(function (req, res) {
            var request = _this.handleRequest(req);
            var response = _this.handleResponse(res);
            var route = _this.routes.find(function (route) {
                return (route.method === req.method || route.method === 'ALL') && route.pathRegexp.test(req.url);
            });
            if (route) {
                console.log(_this.routes);
                console.log(req.url.match(route.pathRegexp).groups);
                request.params = __assign({}, req.url.match(route.pathRegexp).groups);
                route.callback(request, response);
            }
            else {
                res.writeHead(404, { 'Content-Type': 'text/html' });
                res.write('no implemented');
                res.end();
            }
        });
    }
    MyExpress.prototype.listen = function (port, callback) {
        this.server.listen(port, callback);
    };
    MyExpress.prototype.render = function (file, values, cb) {
        var path_file = path_1["default"].join(process.cwd(), 'static', file + ".html");
        if (!fs_1.existsSync(path_file)) {
            cb(new Error('marche pas'), null);
        }
        else {
            var content = fs_1.readFileSync(path_file, 'utf-8');
            cb(null, this.contentProcess(content, values));
        }
    };
    MyExpress.prototype.contentProcess = function (content, values) {
        var _this = this;
        var regex = /{{ ?(\w+)(( ?[|] ?)((\w+)(\:(\d+))?))? ?}}/gi;
        var contentProcessed = content.replace(regex, function (item) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var key = args[0], pipe = args[2], transform = args[4], limit = args[6];
            var v = values[key];
            if (!v)
                return 'undefined';
            if (pipe && transform) {
                var func = _this[transform];
                if (transform === 'fixed')
                    return func(v, limit);
                return func ? func(v) : v;
            }
            else {
                return v;
            }
        });
        return contentProcessed;
    };
    MyExpress.prototype.upper = function (content) {
        return content.toUpperCase();
    };
    MyExpress.prototype.lower = function (content) {
        return content.toLowerCase();
    };
    MyExpress.prototype.fixed = function (content, limit) {
        var tmp = parseFloat(content);
        return tmp.toFixed(limit);
    };
    MyExpress.prototype.handleRequest = function (req) {
        var request = req;
        return request;
    };
    MyExpress.prototype.handleResponse = function (res) {
        var response = res;
        response.json = function (item) {
            response.setHeader('Content-Type', 'application/json');
            response.write(JSON.stringify(item));
            response.end();
        };
        response.send = function (content) {
            response.setHeader('Content-Type', 'text/html');
            response.write(content);
            response.end();
        };
        response.error = function (content) {
            response.setHeader('Content-Type', 'text/html');
            response.write(content);
            response.end();
        };
        return response;
    };
    return MyExpress;
}());
function express() {
    return new MyExpress();
}
exports["default"] = express;
