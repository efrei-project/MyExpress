import express from "./myexpress_module/MyExpress";
import {join} from "path";

let app = express();
const port = 8080;
app.listen(port, () => {
    console.log(`listening on port: ${port}`);
});
app.get('/index', (req, res) =>{
    app.render('home', {
        firstname: "slok",
        lastname: "TRUCMUCHE",
        age: "45.8547845"
    },(err: Error, html: string) =>{
        if (err) {
            res.error(err.message)
        } else {
            res.send(html)
        }
    });

});




app.get('/api', (req, res) => {
    res.json({ hello: 'From API' });
});

app.get('/user/:id', (req, res) => {
    console.log(req.params)
    const { id } = req.params
    res.json({url: 'user', id_requested: id});
});

app.get('/users?limit=42&status=ADMIN', (req, res) => {
    const { users, limit, status } = req.qParams;
    let obj = {
        users,
        limit,
        status
    };
    for (let i = 0; i < limit + 10; i++){
        obj.users.push({
            id: i
        })
    }
    res.json(obj);
    // ...
})



