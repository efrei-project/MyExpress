"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var MyExpress_1 = __importDefault(require("./myexpress_module/MyExpress"));
var app = MyExpress_1["default"]();
var port = 8080;
app.listen(port, function () {
    console.log("listening on port: " + port);
});
app.get('/index', function (req, res) {
    app.render('home', {
        firstname: "slok",
        lastname: "TRUCMUCHE",
        age: "45.8547845"
    }, function (err, html) {
        if (err) {
            res.error(err.message);
        }
        else {
            res.send(html);
        }
    });
});
app.get('/api', function (req, res) {
    res.json({ hello: 'From API' });
});
app.get('/user/:id', function (req, res) {
    console.log(req.params);
    var id = req.params.id;
    res.json({ url: 'user', id_requested: id });
});
app.get('/users?limit=42&status=ADMIN', function (req, res) {
    var _a = req.qParams, users = _a.users, limit = _a.limit, status = _a.status;
    var obj = {
        users: users,
        limit: limit,
        status: status
    };
    for (var i = 0; i < limit + 10; i++) {
        obj.users.push({
            id: i
        });
    }
    res.json(obj);
    // ...
});
